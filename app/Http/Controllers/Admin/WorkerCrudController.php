<?php

namespace App\Http\Controllers\Admin;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Http\Requests\WorkerRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class WorkerCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class WorkerCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Worker::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/worker');
        CRUD::setEntityNameStrings('worker', 'workers');
        CRUD::setTitle('Працівники');
        CRUD::setHeading('Працівники');
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::addColumn([
            'label' => 'ПІБ',
            'type' => 'closure',
            'name' => 'full_name',
            'function' => function ($model) {
                return $model->name . ' ' . $model->surname . ' ' . $model->patronymic;
            },
            'orderable' => true,
            'orderLogic' => function ($query, $column, $columnDirection) {
                return $query->orderBy(DB::raw('CONCAT_WS(" ", name, surname, patronymic)'), $columnDirection);
            }
        ]);

        CRUD::addColumn([
            'label' => 'Вік',
            'type' => 'closure',
            'name' => 'age',
            'function' => function ($model) {
                $birth_date = Carbon::parse($model->birth_date)->format('Y-m-d');
                return Carbon::now()->diffInYears($birth_date);
            },
            'orderable' => true,
            'orderLogic' => function ($query, $column, $columnDirection) {
                if ($columnDirection == 'asc')
                    $columnDirection = 'desc';
                else
                    $columnDirection = 'asc';

                return $query->orderBy('birth_date', $columnDirection);
            }
        ]);

        CRUD::addColumn([
            'label' => 'Дата прийому на роботу',
            'type' => 'date',
            'name' => 'hired_at',
            'attribute' => 'hired_at',
            'orderable' => true,
        ]);

        CRUD::addColumn([
            'label' => 'Номер телефону',
            'type' => 'text',
            'name' => 'phone',
            'attribute' => 'phone',
            'orderable' => false,
        ]);

        CRUD::addColumn([
            'label' => 'Зарплата',
            'type' => 'text',
            'name' => 'start_salary',
            'attribute' => 'start_salary',
            'orderable' => true,
        ]);

        // CRUD::addFilter([
        //     'type' => 'text',
        //     'name' => 'full_name',
        //     'label' => 'ПІБ'
        // ], false, function($value) {
        //     CRUD::addClause('where', DB::raw('CONCAT_WS(" ", name, surname, patronymic)'), 'like', '%' . $value . '%');
        // });


    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(WorkerRequest::class);

        CRUD::setSubheading('Додати працівника.');

        CRUD::addField([
            'label' => 'Прізвище',
            'name' => 'surname',
            'type' => 'text'
        ]);
        CRUD::addField([
            'label' => "Ім'я",
            'name' => 'name',
            'type' => 'text'
        ]);
        CRUD::addField([
            'label' => "По-батькові",
            'name' => 'patronymic',
            'type' => 'text'
        ]);
        CRUD::addField([
            'label' => "Дата народження",
            'name' => 'birth_date',
            'type' => 'date'
        ]);
        CRUD::addField([
            'label' => "Посада",
            'name' => 'position_id',
            'type' => 'select2_from_array',
            'options' => DB::table('positions')->select(['id', 'title'])->pluck('title', 'id')
        ]);
        CRUD::addField([
            'label' => "Дата найму",
            'name' => 'hired_at',
            'type' => 'date'
        ]);
        CRUD::addField([
            'label' => "Зарплата",
            'name' => 'start_salary',
            'type' => 'number'
        ]);
        CRUD::addField([
            'label' => "Номер телефону",
            'name' => 'phone',
            'type' => 'text'
        ]);
        CRUD::addField([
            'label' => "Електронна пошта",
            'name' => 'email',
            'type' => 'text'
        ]);
        CRUD::addField([
            'label' => "Адреса",
            'name' => 'address',
            'type' => 'text'
        ]);
        CRUD::addField([
            'label' => "Фото",
            'name' => 'photo',
            'type' => 'image',
            'prefix' => '/uploads/images/original/'
        ]);
        CRUD::addField([
            'label' => "Начальник",
            'name' => 'boss_id',
            'type' => 'select2_from_array',
            'options' => DB::table('workers')->select(['id', DB::raw('CONCAT_WS(" ", name, surname, patronymic) AS full_name')])->pluck('full_name', 'id')
        ]);
    }

    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
        CRUD::setSubheading('Редагувати дані працівника.');
    }
}
