<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Foundation\Http\FormRequest;

class WorkerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'surname' => 'required|string|min:3|max:20',
            'name' => 'required|string|min:3|max:20',
            'patronymic' => 'required|string|min:3|max:20',
            'birth_date' => 'required|date',
            'position_id' => 'required|integer',
            'hired_at' => 'required|date',
            'start_salary' => 'required|numeric|min:0',
            'phone' => 'nullable|string|regex:/^\+?3?8?(0\d{9})$/',
            'email' => 'nullable|email',
            'address' => 'required|string',
            'photo' => 'nullable',
            'boss_id' => 'nullable|integer',
        ];
    }

    // protected function prepareForValidation()
    // {
    //     dd($this->all());
    // }
}
