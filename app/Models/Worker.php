<?php

namespace App\Models;

use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Backpack\CRUD\app\Models\Traits\CrudTrait;


class Worker extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'workers';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    protected $fillable = [
        'surname',
        'name',
        'patronymic',
        'birth_date',
        'position_id',
        'hired_at',
        'start_salary',
        'phone',
        'email',
        'address',
        'photo',
        'boss_id',
    ];
    // protected $hidden = [];
    protected $dates = [
        'birth_date',
        'hired_at'
    ];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public static function boot()
    {
        parent::boot();
        static::deleting(function ($obj) {
            Storage::disk('public_folder')->delete($obj->photo);
        });
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function position()
    {
        return $this->belongsTo(Position::class);
    }
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */

    public function setPhotoAttribute($value)
    {
        $attribute_name = 'photo';
        $disk = config('backpack.base.root_disk_name');
        $destination_path = 'public/uploads/images';

        if (!file_exists($destination_path . '/original'))
            mkdir($destination_path . '/original', 0777, true);
        if (!file_exists($destination_path . '/thumb'))
            mkdir($destination_path . '/thumb', 0777, true);
        if (!file_exists($destination_path . '/webp'))
            mkdir($destination_path . '/webp', 0777, true);

        if ($value == null) {
            // delete the image from disk
            Storage::disk($disk)->delete('uploads/images/original' . $this->{$attribute_name});
            Storage::disk($disk)->delete('uploads/images/thumb' . $this->{$attribute_name});
            Storage::disk($disk)->delete('uploads/images/webp' . $this->{$attribute_name});

            // set null in the database column
            $this->attributes[$attribute_name] = null;
        }
        if (Str::startsWith($value, 'data:image')) {
            $image = Image::make($value)->encode('jpg', 90);
            $thumb = Image::make($value)->encode('jpg', 90)->fit(500, 500);
            $webp_image = Image::make($value)->encode('webp', 90);

            // 1. Generate a filename.
            $filename = md5($value . time()) . '.jpg';
            $webp_filename = md5($value . time()) . '.webp';

            // 2. Store the image on disk.
            Storage::disk($disk)->put($destination_path . '/original/' . $filename, $image->stream());
            Storage::disk($disk)->put($destination_path . '/thumb/' . $filename, $thumb->stream());
         
            // $image->encode('webp');
            
            Storage::disk($disk)->put($destination_path . '/webp/' . $webp_filename, $webp_image->stream());

            // 3. Delete the previous image, if there was one.
            Storage::disk($disk)->delete('uploads/images/original/' . $this->{$attribute_name});
            Storage::disk($disk)->delete('uploads/images/thumb/' . $this->{$attribute_name});
            Storage::disk($disk)->delete('uploads/images/webp/' . $this->{$attribute_name});

            // $public_destination_path = Str::replaceFirst('public/', '', $destination_path);
            $this->attributes[$attribute_name] = $filename;
        }
    }
}
