<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWorkersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('workers', function (Blueprint $table) {
            $table->id();
            $table->string('surname', 20);
            $table->string('name', 20);
            $table->string('patronymic', 20);
            $table->date('birth_date');
            $table->unsignedBigInteger('position_id');
            $table->date('hired_at');
            $table->float('start_salary', 12, 2);
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
            $table->string('address');
            $table->string('photo')->nullable();
            $table->unsignedBigInteger('boss_id')->nullable();
            $table->timestamps();

            $table->foreign('position_id')->references('id')->on('positions');
            $table->foreign('boss_id')->references('id')->on('workers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('workers');
    }
}
