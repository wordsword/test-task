<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('dashboard') }}"><i class="la la-home nav-icon"></i> Головна</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('position') }}'><i class='nav-icon la la-question'></i> Посади</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('worker') }}'><i class='nav-icon la la-question'></i> Працівники</a></li>